import React, { useEffect, useState, createContext } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import Home from './pages/home';
import { GlobalStyle } from './globalStyles';

// Style theme that can be used in all the app thanks to ThemeProvider
const theme = {
  mainFontFamily: 'Avenir',
  white: "#FFFFFF",
  green: "#4AAA97",
  lightGreen: '#E5F7F5',
  grey: '#2F4F4F',
  lightGrey: '#E7E7E7',
  orange: '#F9B034',
  red: '#EA6565'
};

export const PlatformContext = createContext();

const App = () => {
  const [platform, setPlatform] = useState('desktop');

  useEffect(() => {
    const handleResize = () => {
      let width = window.innerWidth;

      if (width < 520) {
        return setPlatform('mobile');
      }
      if (width >= 520 && width < 1000) {
        return setPlatform('tablet');
      }
      return setPlatform('desktop');
    };

    handleResize();
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <PlatformContext.Provider value={[platform, setPlatform]}>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Home />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </PlatformContext.Provider>
  );
}

export default App;
