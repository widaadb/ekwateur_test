import axios from 'axios';
import config from '../config/endpoints.json';

const env = process.env.NODE_ENV || 'development';
const urlMeters = config[env].meters;
const urlPod = config[env].pod;

export const getMeters = async () => {
  try {
    const res = await axios.get(`${urlMeters}`);
    if (res) {
      return res;
    }
    return null;
  } catch(error) {
    console.error(error);
  }
};

export const getPod = async ({ pod }) => {
  try {
    const res = await axios.get(`${urlPod}/${pod}`);
    if (res) {
      return res;
    }
    return null;
  } catch(error) {
    console.error(error);
  }
};