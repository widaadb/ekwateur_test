const tableSchema = {
  electricity: {
    id: 1,
    titles: [
      {
        id: 1,
        name: 'date',
        text: 'Date',
        filter: true,
        sort: false
      },
      {
        id: 2,
        name: 'valueHP',
        text: 'Heures pleines',
        smallText: 'HP',
        filter: false,
        sort: true
      },
      {
        id: 3,
        name: 'valueHC',
        text: 'Heures creuses',
        smallText: 'HC',
        filter: false,
        sort: true
      }
    ]
  },
  gas: {
    id: 1,
    titles: [
      {
        id: 1,
        name: 'date',
        text: 'Date',
        filter: true,
        sort: false
      },
      {
        id: 2,
        name: 'value',
        text: 'Valeur',
        smallText: 'Valeur',
        filter: false,
        sort: true
      }
    ]
  }
};

export default tableSchema;