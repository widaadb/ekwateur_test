import styled from 'styled-components';
import { FlexDivCenter } from '../../globalStyles';

export const ContainerTable = styled(FlexDivCenter)`
  padding: 20px 20px 40px;
`;

export const TableStyle = styled.table`
  border-collapse: collapse;
  border-radius: 1em;
  overflow: hidden;
  box-shadow: rgb(0 0 0 / 10%) 0px 2px 33px 0px;
`;

export const TdStyle = styled.td`
  padding: 1em;
  width: 130px;
  background: ${props => props.theme.lightGreen};
  border-right: 2px solid white;
  text-align: center;
  font-family: ${props => props.theme.mainFontFamily};
  color: ${props => props.theme.grey};
  height: 25px;

  &:last-child {
    border-right: none;
  }

  @media (max-width: 520px) {
    padding: 0.7em;
    font-size: 15px;
  }
`;

export const ThStyle = styled.th`
  width: 130px;
  background: ${props => props.theme.lightGreen};
  border-bottom: 2px solid white;
  border-right: 2px solid white;
  text-align: center;
  font-family: ${props => props.theme.mainFontFamily};
  font-style: italic;
  color: ${props => props.theme.green};
  height: 25px;
  cursor: pointer;
  padding: 0;

  &:last-child {
    border-right: none;
  }
`;

export const TrStyle = styled.tr`
  border-bottom: 2px solid white;

  &:last-child {
    border-bottom: none;
  }
`;

export const ButtonSort = styled.button`
  color: inherit;
  font-size: inherit;
  border: none;
  background-color: inherit;
  font-family: inherit;
  font-weight: inherit;
  font-style: inherit;
  cursor: pointer;
  height: 100%;
  width: 100%;
  padding: 0.5em 0;

  @media (max-width: 505px) {
    max-width: 120px;
  }
`;

export const ContainerError = styled(FlexDivCenter)`
  margin-top: 70px;
`;

export const TextError = styled.p`
  font-family: ${props => props.theme.mainFontFamily};
  color: ${props => props.theme.red} !important;
  font-weight: bold;
  text-align: center;
`;