import React, { useContext } from 'react';
import Select from 'react-select'
import moment from 'moment';
import { PlatformContext }  from '../../App';
import { ContainerError, ContainerTable, TableStyle, TdStyle, TextError, ThStyle, TrStyle, ButtonSort } from './styles';

const Table = ({ template, data, arrOfYears, handleSelect, yearSelected, handleClickSort, sortArrow }) => {
  const [platform, ] = useContext(PlatformContext);

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: '#4AAA97',
      backgroundColor: state.isSelected ? 'rgba(249, 176, 52, 0.4)' : state.isFocused ? '#E5F7F5' : '#FFFFFF',
      cursor: 'pointer'
    }),
    control: (provided, state) => ({
      ...provided,
      backgroundColor: 'inherit',
      display: 'flex',
      color: '#4AAA97',
      padding: 0,
      boxShadow: 'none',
      cursor: 'pointer',
      border: 'none',
      outline: state.isFocused ? '1px solid #4AAA97' : '0',
      borderRadius: '0.9em',
      marginTop: '1px',
      marginLeft: '1px'
    }),
    singleValue: (provided) =>( {
      ...provided,
      color: '#4AAA97'
    }),
    dropdownIndicator: base => ({
      ...base,
      color: '#4AAA97'
    })
  };

  return (
    <ContainerTable>
      {data.length ? (
        <TableStyle>
          <thead>
            <tr>
              {template.titles.map(title => {
                if (title.filter) {
                  return (
                    <ThStyle key={title.id}>
                      <Select
                        options={arrOfYears}
                        styles={customStyles}
                        onChange={(e) => handleSelect(e)}
                        defaultValue={{value: 'all', label: 'Dates'}}
                        blurInputOnSelect
                      />
                    </ThStyle>
                  )
                }
                return (
                  <ThStyle key={title.id}>
                    {title.sort ? (
                      <ButtonSort
                        aria-label={'Trier les valeurs'}
                        onClick={() => handleClickSort({
                          value: title.name,
                          arrow: sortArrow[title.name].arrow === '▼' ? '▲' : '▼',
                          sort: sortArrow[title.name].sort === 'asc' ? 'desc' : 'asc'
                        })}
                      >
                        {platform === 'mobile' ? `${title.smallText} ${sortArrow[title.name].arrow}` :
                        `${title.text} ${sortArrow[title.name].arrow}`}
                      </ButtonSort>)
                    : title.text}
                  </ThStyle>
                )
              })}
            </tr>
          </thead>
          <tbody>
            {yearSelected.value === 'all' ? (
              data.map((element) => {
                return (
                  <TrStyle key={element.id}>
                    <TdStyle>{moment(element.date).format('DD/MM/YYYY')}</TdStyle>
                    {element.valueHP ? (
                      <>
                        <TdStyle>{element.valueHP} kWh</TdStyle>
                        <TdStyle>{element.valueHC} kWh</TdStyle>
                      </>
                    ) : (
                      <TdStyle>{element.value} kWh</TdStyle>
                    )}
                  </TrStyle>
                )
              })
            ) : (
              data.filter(el => moment(el.date).year() === Number(yearSelected.value)).map((element) => {
                return (
                  <TrStyle key={element.id}>
                    <TdStyle>{moment(element.date).format('DD/MM/YYYY')}</TdStyle>
                    {element.valueHP ? (
                      <>
                        <TdStyle>{element.valueHP} kWh</TdStyle>
                        <TdStyle>{element.valueHC} kWh</TdStyle>
                      </>
                    ) : (
                      <TdStyle>{element.value} kWh</TdStyle>
                    )}
                  </TrStyle>
                )
              })
            )
          }
          </tbody>
        </TableStyle>
      ) : (
        <ContainerError>
          <TextError>
            Une erreur s'est produite, merci de contacter <a href="https://ekwateur.fr/contact/" target="_blank" rel="noreferrer">notre service clients.</a>
          </TextError>
        </ContainerError>
      )}
    </ContainerTable>
  )
};

export default Table;