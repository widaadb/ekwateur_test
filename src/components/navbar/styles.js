import styled from 'styled-components';

export const NavContainer = styled.nav`
  display: flex;
  justify-content: center;
  background-color: ${props => props.theme.white};
  width: 100%;
  height: 80px;
  box-shadow: rgb(225 225 225 / 36%) 0px 9px 20px 0px;
  position: fixed;
  top: 0;
  z-index: 10;
`;

export const LogoContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const Logo = styled.img`
  width: 200px;
`;