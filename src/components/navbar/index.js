import React from 'react';
import logo from '../../assets/logo-ekWateur.png';
import { Logo, LogoContainer, NavContainer } from './styles';

const Navbar = () => {
  return (
    <NavContainer>
      <LogoContainer>
        <a href="https://ekwateur.fr" target="_blank" rel="noreferrer">
          <Logo alt="ekWateur logo" src={logo} />
        </a>
      </LogoContainer>
    </NavContainer>
  )
};

export default Navbar;
