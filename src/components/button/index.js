import React from 'react';
import { ContainerButton, ContentButton, ImgButton } from './styles';

const Button = ({ value, text, handleClickChoice, energyChoice, icon }) => {
  return (
      <ContainerButton
        onClick={() => handleClickChoice({ choice: value })}
        isSelected={value === energyChoice}>
        <ContentButton>
          <ImgButton src={require(`../../assets/${icon}`).default} alt={`${value} icon`}/>
          {text}
        </ContentButton>
      </ContainerButton>
  )
};

export default Button;