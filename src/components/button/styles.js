import styled from 'styled-components';
import { FlexDivCenter } from '../../globalStyles';

export const ContainerButton = styled.button`
  border-radius: 6px;
  height: 50px;
  width: 130px;
  background-color: ${props => props.isSelected ? props.theme.green : props.theme.white};
  color:  ${props => props.isSelected ? props.theme.white : props.theme.green};
  border:  ${props => props.isSelected ? 'none' : `1px solid ${props.theme.green}`};
  cursor: pointer;
  transition: 0.4s;
  font-family: ${props => props.theme.mainFontFamily};
  font-weight: bold;
`;

export const ContentButton = styled(FlexDivCenter)`
  width: 100%;
  justify-content: space-evenly;
`;

export const ImgButton = styled.img`
  width: 30px;
`;