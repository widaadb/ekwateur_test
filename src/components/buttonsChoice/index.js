import React from 'react';
import Button from '../button';
import { ContainerButtons } from './styles';

const ButtonsChoice = ({ handleClickChoice, energyChoice }) => {

  const buttonsList = [
    {
      id: 1,
      value: 'electricity',
      icon: 'electricity.png',
      text: 'Éléctricité'
    },
    {
      id: 2,
      value: 'gas',
      icon: 'gas.png',
      text: 'Gaz',
    }
  ];

  return (
    <ContainerButtons>
      {buttonsList.map((button) => {
        return (
          <Button
            key={button.id}
            value={button.value}
            text={button.text}
            icon={button.icon}
            handleClickChoice={handleClickChoice}
            energyChoice={energyChoice}
          />
        )
      })}
    </ContainerButtons>
  )
};

export default ButtonsChoice;