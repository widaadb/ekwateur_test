import styled from 'styled-components';
import { FlexDivCenter } from '../../globalStyles';

export const ContainerButtons = styled(FlexDivCenter)`
  gap: 12px;
  margin: 10px 0 20px 0;
`;