import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { getMeters, getPod } from '../../controllers/fetch';
import ButtonsChoice from '../../components/buttonsChoice';
import Navbar from '../../components/navbar';
import Table from '../../components/table';
import tableSchema from '../../controllers/tableSchema';
import { ContainerLoader, HomeContainer, MainTitle, ButtonsTitle, LoaderImg } from './styles';
import loader from '../../assets/loader.svg';

const Home = () => {
  const [energyChoice, setEnergyChoice] = useState('electricity');
  const [dataEnergy, setDataEnergy] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [arrOfYears, setArrOfYears] = useState([]);
  const [yearSelected, setYearSelected] = useState([]);
  const [sortArrow, setSortArrow] = useState({
    valueHP: {
      arrow: '▼', sort: 'asc'
    },
    valueHC: {
      arrow: '▼', sort: 'asc'
    },
    value: {
      arrow: '▼', sort: 'asc'
    }
  });

  useEffect(() => {
    const getData = async () => {
      setIsLoaded(false);
      try {
        let arrToReturn = [{ value: 'all', label: 'Dates' }];
        let meters = await getMeters();
        let findEnergy = (element) => element.energy === energyChoice;
        // Get the point of delivery within the energy to get the good values
        let pod = meters.data.find(findEnergy).pointOfDelivery;
        let res = await getPod({ pod });
        res.data.map((element) => {
          // Get years from the dates, and push them only once in an array to get every years so we can filter by year
          const yearFound = arrToReturn.find(el => el.value === moment(element.date).year())
          if (yearFound) {
            return null;
          }
          return arrToReturn.push({value: moment(element.date).year(), label: moment(element.date).year()})
        });
        setDataEnergy(res.data.sort((a, b) => a.date > b.date ? -1 : 1));
        setArrOfYears([...arrToReturn]);
        setYearSelected({ value: 'all', label: 'Dates' });
        return setIsLoaded(true);
      } catch(error) {
        console.error(error);
        setDataEnergy([]);
        return setIsLoaded(true);
      }
    };
    getData();
  }, [energyChoice]);

  const handleClickChoice = ({ choice }) => {
    setEnergyChoice(choice);
  };

  // Function to filter by year
  const handleSelect = (e) => {
    e.value === 'all' ? setYearSelected({value: e.value, label: 'Dates'}) : setYearSelected({value: e.value, label: e.value});
    dataEnergy.sort((a, b) => a.date > b.date ? -1 : 1);
  };

  // Function to sort data
  const handleClickSort = ({ value, arrow, sort }) => {
    setSortArrow({
      ...sortArrow,
      [value]: {
        arrow,
        sort
      }
    });

    if (sort === 'asc') {
      return dataEnergy.sort((a, b) => a[value] > b[value] ? -1 : 1);
    }
    return dataEnergy.sort((a, b) => a[value] > b[value] ? 1 : -1);
  };

  return (
    <HomeContainer>
      <Navbar />
      <MainTitle>Bienvenue dans votre espace de consommation</MainTitle>
      <ButtonsTitle>Cliquez sur le type d'énergie souhaité:</ButtonsTitle>
      <ButtonsChoice
        energyChoice={energyChoice}
        handleClickChoice={handleClickChoice}
      />
      {isLoaded ? (
        <Table
          template={tableSchema[energyChoice]}
          data={dataEnergy}
          arrOfYears={arrOfYears}
          handleSelect={handleSelect}
          yearSelected={yearSelected}
          handleClickSort={handleClickSort}
          sortArrow={sortArrow}
        />
      ) : (
        <ContainerLoader>
          <LoaderImg src={loader} alt="loader" />
        </ContainerLoader>
      )}
    </HomeContainer>
  )
};

export default Home;