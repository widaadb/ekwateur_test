import styled from 'styled-components';
import { FlexDivCenter } from '../../globalStyles';

export const HomeContainer = styled.div`
  height: 100%;
`;

export const MainTitle = styled.h1`
  font-family: ${props => props.theme.mainFontFamily};
  color: ${props => props.theme.green};
  text-align: center;
  font-size: 30px;
  margin-left: 10px;
  margin-right: 10px;
  margin-top: 120px;

  @media (max-width: 520px) {
    font-size: 25px;
  }
`;

export const ButtonsTitle = styled.h2`
  font-family: ${props => props.theme.mainFontFamily};
  color: ${props => props.theme.orange};
  text-align: center;
  font-size: 20px;
  font-style: italic;
  margin: 0 10px;

  @media (max-width: 520px) {
    font-size: 17px;
  }
`;

export const ContainerLoader = styled(FlexDivCenter)`
  margin-top: 140px;
`;

export const LoaderImg = styled.img`
  width: 110px;
`;