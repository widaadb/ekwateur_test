import styled, { createGlobalStyle } from 'styled-components';

export const FlexDivCenter = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const GlobalStyle = createGlobalStyle`
  html {
    height: 100%;
    cursor: default;
  }

  body {
    height: 100%;
    margin: 80px 0 0 0;
  }

  a {
    color: unset;
  }
`;